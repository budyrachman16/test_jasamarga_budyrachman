// employeeRoutes.js
import express from 'express';
import { Employee } from '../model/employee.js'; 


const router = express.Router();

// Get All Employees
router.get('/all', async (req, res) => {
    try {
        const employees = await Employee.findAll();
        res.json(employees);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Get One Employee
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const employee = await Employee.findByPk(id);
        if (!employee) {
            res.status(404).json({ message: 'Employee not found' });
        } else {
            res.json(employee);
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Create Employee
router.post('/', async (req, res) => {
    const { nik, name } = req.body;
    try {
        const employee = await Employee.create({ nik, name });
        res.status(201).json(employee);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

// Update Employee
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { nik, name } = req.body;
    try {
        const employee = await Employee.findByPk(id);
        if (!employee) {
            res.status(404).json({ message: 'Employee not found' });
        } else {
            employee.nik = nik;
            employee.name = name;
            await employee.save();
            res.json(employee);
        }
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

// Delete Employee
router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const employee = await Employee.findByPk(id);
        if (!employee) {
            res.status(404).json({ message: 'Employee not found' });
        } else {
            await employee.destroy();
            res.json({ message: 'Employee deleted successfully' });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

export default router;
