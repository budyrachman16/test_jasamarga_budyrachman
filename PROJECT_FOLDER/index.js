import express from 'express'
import bodyParser from 'body-parser';
import { connection } from './postgrsql.js';
import employeeRoutes from './routes/employeeRoutes.js';
import { client } from './koneksi.js';

const app=express();
const PORT = 3000;
connection();


app.use(bodyParser.json())

app.listen(PORT, ()=>{
    console.log(`server  in running at ${PORT}`);
});


client.connect(err => {
    if (err) {
        console.log(err.message)
    }else{
        console.log('connected')
    }
})



app.get('/employees/all', (req, res) => {
    client.query(`Select * from employees`, (err, result) => {
        if (!err) {
            res.send(result);
        }
    });
});


app.get('/employees/:id', (req, res) => {
    const employeeId = req.params.id;

    client.query(
        `SELECT employees.*, education.name AS education_name, education.level AS education_level, education.description AS education_description, 
                employee_profiles.place_of_birth, employee_profiles.date_of_birth, employee_profiles.gender, employee_profiles.is_married, employee_profiles.prof_pict,
                employee_family.name AS family_name, employee_family.identifier, employee_family.job, employee_family.palace_of_birth AS family_place_of_birth, employee_family.date_of_birth AS family_date_of_birth,
                employee_family.religion AS family_religion, employee_family.is_life, employee_family.is_divoreced, employee_family.relation_status
         FROM employees
         LEFT JOIN education ON employees.id = education.employee_id
         LEFT JOIN employee_profiles ON employees.id = employee_profiles.employee_id
         LEFT JOIN employee_family ON employees.id = employee_family.employee_id
         WHERE employees.id = $1`,
        [employeeId],
        (err, result) => {
            if (err) {
                res.status(500).json({ error: err.message });
                return;
            }

            if (result.rows.length === 0) {
                res.status(404).json({ message: 'Employee not found' });
                return;
            }

            const employeeData = {
                id: result.rows[0].id,
                nik: result.rows[0].nik,
                name: result.rows[0].name,
                is_active: result.rows[0].is_active,
                start_date: result.rows[0].start_date,
                end_date: result.rows[0].end_date,
                created_at: result.rows[0].created_at,
                updated_at: result.rows[0].updated_at,
                education: {
                    name: result.rows[0].education_name,
                    level: result.rows[0].education_level,
                    description: result.rows[0].education_description
                },
                profile: {
                    place_of_birth: result.rows[0].place_of_birth,
                    date_of_birth: result.rows[0].date_of_birth,
                    gender: result.rows[0].gender,
                    is_married: result.rows[0].is_married,
                    prof_pict: result.rows[0].prof_pict
                },
                family: result.rows.map(row => ({
                    name: row.family_name,
                    identifier: row.identifier,
                    job: row.job,
                    palace_of_birth: row.family_place_of_birth,
                    date_of_birth: row.family_date_of_birth,
                    religion: row.family_religion,
                    is_life: row.is_life,
                    is_divoreced: row.is_divoreced,
                    relation_status: row.relation_status
                }))
            };

            res.json(employeeData);
        }
    );
});



app.post('/employees', (req, res) => {
    const { nik, name, is_active, start_date, end_date, place_of_birth, date_of_birth, gender, is_married, prof_pict, family, education } = req.body;

    client.query('BEGIN', async (err) => {
        try {
            if (err) throw err;

            const employeeResult = await client.query(
                `INSERT INTO employees (nik, name, is_active, start_date, end_date, created_at, updated_at)
                 VALUES ($1, $2, $3, $4, $5, NOW(), NOW()) RETURNING id`,
                [nik, name, is_active, start_date, end_date]
            );

            const employeeId = employeeResult.rows[0].id;

            await client.query(
                `INSERT INTO employee_profiles (employee_id, place_of_birth, date_of_birth, gender, is_married, prof_pict, created_by, updated_by, created_at, updated_at)
                 VALUES ($1, $2, $3, $4, $5, $6, 'Admin', 'Admin', NOW(), NOW())`,
                [employeeId, place_of_birth, date_of_birth, gender, is_married, prof_pict]
            );

            if (family && family.length > 0) {
                const familyValues = family.map(member => `(${employeeId}, '${member.name}', '${member.identifier}', '${member.job}', '${member.palace_of_birth}', '${member.date_of_birth}', '${member.religion}', ${member.is_life}, ${member.is_divoreced}, '${member.relation_status}', 'Admin', 'Admin', NOW(), NOW())`).join(',');
                await client.query(
                    `INSERT INTO employee_family (employee_id, name, identifier, job, palace_of_birth, date_of_birth, religion, is_life, is_divoreced, relation_status, created_by, updated_by, created_at, updated_at)
                     VALUES ${familyValues}`
                );
            }

            if (education && education.length > 0) {
                const educationValues = education.map(edu => `(${employeeId}, '${edu.name}', '${edu.level}', '${edu.description}', 'Admin', 'Admin', NOW(), NOW())`).join(',');
                await client.query(
                    `INSERT INTO education (employee_id, name, level, description, created_by, updated_by, created_at, updated_at)
                     VALUES ${educationValues}`
                );
            }

            await client.query('COMMIT');

            res.status(201).json({ message: 'Employee created successfully' });
        } catch (error) {
            await client.query('ROLLBACK');
            console.error('Error creating employee:', error);
            res.status(500).json({ error: 'Failed to create employee' });
        }
    });
});




app.put('/employees/:id', (req, res) => {
    const employeeId = req.params.id;
    const { nik, name, is_active, start_date, end_date } = req.body;
    const { place_of_birth, date_of_birth, gender, is_married, prof_pict } = req.body.profile;
    const family = req.body.family;
    const education = req.body.education;


    client.query('BEGIN', async (err) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }

        try {
   
            await client.query('UPDATE employees SET nik=$1, name=$2, is_active=$3, start_date=$4, end_date=$5 WHERE id=$6', [nik, name, is_active, start_date, end_date, employeeId]);
            await client.query('UPDATE employee_profiles SET place_of_birth=$1, date_of_birth=$2, gender=$3, is_married=$4, prof_pict=$5 WHERE employee_id=$6', [place_of_birth, date_of_birth, gender, is_married, prof_pict, employeeId]);
            for (const member of family) {
                await client.query('UPDATE employee_family SET name=$1, identifier=$2, job=$3, palace_of_birth=$4, date_of_birth=$5 WHERE employee_id=$6 AND id=$7', [member.name, member.identifier, member.job, member.palace_of_birth, member.date_of_birth, employeeId, member.id]);
            }

            for (const edu of education) {
                await client.query('UPDATE education SET name=$1, level=$2, description=$3 WHERE employee_id=$4 AND id=$5', [edu.name, edu.level, edu.description, employeeId, edu.id]);
            }

            await client.query('COMMIT');
            res.status(200).json({ message: 'Employee updated successfully' });
        } catch (error) {
            await client.query('ROLLBACK');
            res.status(500).json({ error: error.message });
        }
    });
});



app.delete('/employees/:id', (req, res) => {
    const employeeId = req.params.id;
    

    client.query('DELETE FROM employees WHERE id = $1', [employeeId], (err, result) => {
        if (err) {
            res.status(500).json({ error: err.message });
        } else {
            client.query('DELETE FROM education WHERE employee_id = $1', [employeeId], (err, result) => {
                if (err) {
                    res.status(500).json({ error: err.message });
                } else {
                    client.query('DELETE FROM employee_profiles WHERE employee_id = $1', [employeeId], (err, result) => {
                        if (err) {
                            res.status(500).json({ error: err.message });
                        } else {
                            client.query('DELETE FROM employee_family WHERE employee_id = $1', [employeeId], (err, result) => {
                                if (err) {
                                    res.status(500).json({ error: err.message });
                                } else {
                                    res.status(200).json({ message: `Employee with ID ${employeeId} and related data deleted successfully` });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});


app.get('/employees/report/all', (req, res) => {
    client.query(`
        SELECT e.id, e.nik, e.name, e.is_active, 
               ep.gender, 
               date_part('year', age(ep.date_of_birth)) || ' years old' as age, 
               ed.name as education_name, ed.level as education_level, 
               COUNT(CASE WHEN ef.relation_status = 'istri' THEN 1 END) || ' istri & ' || 
               COUNT(CASE WHEN ef.relation_status = 'anak' THEN 1 END) || ' anak' as family_info
        FROM employees e
        LEFT JOIN employee_profiles ep ON e.id = ep.employee_id
        LEFT JOIN education ed ON e.id = ed.employee_id
        LEFT JOIN employee_family ef ON e.id = ef.employee_id
        GROUP BY e.id, e.nik, e.name, e.is_active, ep.gender, ep.date_of_birth, ed.name, ed.level
    `, (err, result) => {
        if (!err) {
            res.send(result.rows);
        } else {
            console.error('Error fetching employee data:', err);
            res.status(500).json({ error: 'Failed to fetch employee data' });
        }
    });
});

