import { DataTypes, Sequelize } from "sequelize";

export const employee_profile_Model=(sequelize)=>{
    const {DataTypes}=Sequelize;

    return sequelize.define("employee_profile", {
        employee_id:{
            type:DataTypes.INTEGER
        },
        place_of_birth:{
            type:DataTypes.STRING,
            allowNull: false,
        },
        date_of_birth:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        gender:{
            type:DataTypes.ENUM('laki-laki', 'perempuan'),
            allowNull: true,
        },
        is_married:{
            type:DataTypes.BOOLEAN,
            allowNull: true,
        },
        prof_pict:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        updated_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        updated_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        }
    });
}