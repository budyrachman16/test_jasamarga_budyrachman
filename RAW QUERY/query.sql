INSERT INTO employees (nik, name, is_active, start_date, end_date, created_at, updated_at)
VALUES (11012, 'Budi', TRUE, '2022-12-12', '2029-12-12', NOW(), NOW()),
       (11013, 'Jarot', TRUE, '2021-09-01', '2028-09-01', NOW(), NOW());
	   
	   
INSERT INTO education (employee_id, name, level, description, created_by, updated_by, created_at, updated_at)
VALUES (1, 'SMKN 7 Jakarta', 'SMA', 'Sekolat Menengah Atas', 'Admin','Admin', '2022-12-12', '2022-12-12'),
       (2, 'Universitas Negri Jakarta', 'Strata 1', 'Sarjana','Admin','Admin', '2022-12-12', '2022-12-12');
	   
   
INSERT INTO employee_profiles (employee_id, place_of_birth, date_of_birth, gender, is_married, prof_pict, created_by, updated_by, created_at, updated_at)
VALUES (1, 'Jakarta',  '1997-05-02', 'laki-laki','true','', 'Admin','Admin', '2022-12-12', '2022-12-12'),
       (2, 'Sukabumi', '1997-05-02', 'laki-laki','false','', 'Admin','Admin', '2022-12-12', '2022-12-12');
	   
	   
INSERT INTO employee_family (employee_id, name, identifier, job, palace_of_birth, date_of_birth, religion, is_life, is_divoreced, relation_status, created_by, updated_by, created_at, updated_at)
VALUES (1, 'Marni',  '32100594109960002', 'Ibu Rumah Tangga','Denpasar','1995-10-17','islam','TRUE','FALSE','istri', 'Admin','Admin', '2022-12-12', '2022-12-12'),
       (1, 'Clara',  '32100594109020004', 'Pelajar','Bangkalan','2008-10-17','islam','TRUE','FALSE','anak', 'Admin','Admin', '2022-12-12', '2022-12-12'),
	   (1, 'Stephanie',  '32100594109020005', 'Pelajar','Bangkalan','2008-10-17','islam','TRUE','FALSE','anak', 'Admin','Admin', '2022-12-12', '2022-12-12');
	   
	   
	   
select * from employee_profiles;
select * from employee_family;
select * from employees;
select * from education;



		 
SELECT employees.id, employees.nik, employees.name, employees.is_active, 
       employee_profiles.gender, 
       date_part('year', age(employee_profiles.date_of_birth)) || ' years old' as age, 
       education.name as education_name, education.level as education_level, 
       COUNT(CASE WHEN employee_family.relation_status = 'istri' THEN 1 END) || ' istri & ' || 
       COUNT(CASE WHEN employee_family.relation_status = 'anak' THEN 1 END) || ' anak' as family_info
FROM employees
LEFT JOIN employee_profiles ON employees.id = employee_profiles.employee_id
LEFT JOIN education ON employees.id = education.employee_id
LEFT JOIN employee_family ON employees.id = employee_family.employee_id
GROUP BY employees.id, employees.nik, employees.name, employees.is_active, employee_profiles.gender, employee_profiles.date_of_birth, education.name, education.level;

