import { Sequelize } from 'sequelize';

// Buat instance Sequelize
export const sequelize = new Sequelize('data_kepegawaian','postgres','123',{
    host: 'localhost',
    dialect: 'postgres',
    logging: false, 
});


// Fungsi untuk menguji koneksi ke database
export const testConnection = async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection to the database has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
};

// Ekspor instance Sequelize untuk digunakan di seluruh aplikasi Anda
export default sequelize;
