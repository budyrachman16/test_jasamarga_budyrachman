import { Sequelize } from "sequelize";
import { employee_profile_Model } from "./model/employee_profile.js";
import { Employee } from "./model/employee.js";
import { education_model } from "./model/education.js";
import { employee_family_Model } from "./model/employee_family.js";



export const connection= async()=>{
    const sequelize = new Sequelize('data_kepegawaian','postgres','123',{
        host: 'localhost',
        dialect: 'postgres',
    });

let employee_profile = null;
let employee = null;
let education = null;
let employee_family = null;

    try {
        await sequelize.authenticate();
        console.log("Connection success");

        employee_profile = employee_profile_Model(sequelize);
        employee = Employee(sequelize);
        education = education_model(sequelize);
        employee_family = employee_family_Model(sequelize);
        await sequelize.sync();
        console.log("Connection 1");
        
    } catch (error) {
        console.error("unable to connect");
        
    }
}

