import { DataTypes, Sequelize } from "sequelize";
import { sequelize } from "../database.js";

export const Employee=(sequelize)=>{
    const {DataTypes}=Sequelize;

    return sequelize.define("employee", {
        nik:{
            type:DataTypes.INTEGER
        },
        name:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        is_active:{
            type:DataTypes.BOOLEAN,
            allowNull: true,
        },
        start_date:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        end_date:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        created_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        updated_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        updated_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        }
    });    
}