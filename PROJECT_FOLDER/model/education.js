import { DataTypes, Sequelize } from "sequelize";

export const education_model=(sequelize)=>{
    const {DataTypes}=Sequelize;

    return sequelize.define("education", {
        employee_id:{
            type:DataTypes.INTEGER
        },
        name:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        level:{
            type:DataTypes.ENUM('TK','SD','SMP','SMA','Strata 1','Strata 2','Doktor','prof'),
            allowNull: true,
        },
        description:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        updated_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        updated_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        }
    });
}