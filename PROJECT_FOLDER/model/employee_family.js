import { DataTypes, Sequelize } from "sequelize";

export const employee_family_Model=(sequelize)=>{
    const {DataTypes}=Sequelize;

    return sequelize.define("employee_family", {
        employee_id:{
            type:DataTypes.INTEGER
        },
        name:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        identifier:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        job:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        palace_of_birth:{
            type:DataTypes.STRING,
            allowNull: false,
        },
        date_of_birth:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        religion:{
            type:DataTypes.ENUM('islam','katolik','buda','protestan','konghucu'),
            allowNull: true,
        },
        is_life:{
            type:DataTypes.BOOLEAN,
            allowNull: true,
        },
        is_divoreced:{
            type:DataTypes.BOOLEAN,
            allowNull: true,
        },
        relation_status:{
            type:DataTypes.ENUM('suami','istri','anak','anak sambung'),
            allowNull: true,
        },
        created_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        updated_by:{
            type:DataTypes.STRING,
            allowNull: true,
        },
        created_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        },
        updated_at:{
            type:DataTypes.DATEONLY,
            allowNull: true,
        }
    });
}